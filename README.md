> repo moved to [git.ant.lgbt/experiments/desktop/wpf-calculator](https://git.ant.lgbt/experiments/desktop/wpf-calculator)

This is a calculator I wrote in C# and WPF that allows basic calculations. I'm not going to be continuing to work on it though, because it is based on WPF and can't really run under linux.