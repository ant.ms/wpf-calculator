﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Taschenrechner {
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        long lastResult = 0;
        long result = 0;
        char operation = '+';

        private void displayLastResult() {
            Output_TextBox.Text = lastResult.ToString();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            switch (operation) {
                case '+': {
                        result += lastResult;
                        break;
                    }
                case '-': {
                        result -= lastResult;
                        break;
                    }
                case '*': {
                        result *= lastResult;
                        break;
                    }
                case '/': {
                        result /= lastResult;
                        break;
                    }
                default: {
                        break;
                    }
            }
            lastResult = result;
            displayLastResult();
        }

        private void Nine_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 9;
            displayLastResult();

        }

        private void Eight_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 8;
            displayLastResult();

        }
        private void Seven_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 7;
            displayLastResult();

        }

        private void Six_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 6;
            displayLastResult();

        }

        private void Five_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 5;
            displayLastResult();

        }

        private void Four_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 4;
            displayLastResult();

        }
        private void Three_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 3;
            displayLastResult();

        }

        private void Two_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 2;
            displayLastResult();

        }

        private void One_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10 + 1;
            displayLastResult();

        }

        private void Zero_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = lastResult * 10;
            displayLastResult();
        }


        private void Clear_Button_Click(object sender, RoutedEventArgs e) {
            lastResult = 0;
            displayLastResult();
        }

        private void Divide_Button_Click(object sender, RoutedEventArgs e) {
            result = lastResult;
            operation = '/';
            lastResult = 0;
            displayLastResult();

        }

        private void Multiply_Button_Click(object sender, RoutedEventArgs e) {
            result = lastResult;
            operation = '*';
            lastResult = 0;
            displayLastResult();

        }

        private void Minus_Button_Click(object sender, RoutedEventArgs e) {
            result = lastResult;
            operation = '-';
            lastResult = 0;
            displayLastResult();

        }

        private void Plus_Button_Click(object sender, RoutedEventArgs e) {
            result = lastResult;
            operation = '+';
            lastResult = 0;
            displayLastResult();
        }

        private void Radio_Button_Click(object sender, RoutedEventArgs e) {

        }
    }
}
